//!
//! Quasigroup modules.
//!
//! The `quasigroup` module provides sub-modules for abstract algebraic
//! _quasigroups_.
//!
//! The module does not provide implementations on the Rust numeric
//! types to avoid over-specializing "multiplication" and "division" for
//! third party modules.
//!
pub mod quasigroup;


// Make sub-modules visible in this module.
pub use self::quasigroup::*;
