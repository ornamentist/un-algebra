//!
//! Additive semigroups.
//!
//! An algebraic _additive_ _semigroup_ is an _additive_ _magma_ `M`,
//! where the `addition` operation `+` is _associative_.
//!
//! # Axioms
//!
//! ```text
//! ∀x, y, z ∈ M
//!
//! Associativity: (x + y) + z = x + (y + z).
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive semigroup.
//!
#![doc(include = "../../doc/references.md")]

use crate::magma::*;
use crate::numeric::*;


///
/// An algebraic _additive semigroup_.
///
pub trait AddSemigroup: AddMagma {}


///
/// Laws of additive semigroups.
///
pub trait AddSemigroupLaws: AddSemigroup {

  /// The additive _associativity_ axiom.
  fn associativity(&self, x: &Self, y: &Self) -> bool {
      self.add(&x.add(y)) == self.add(x).add(y)
  }
}


///
/// Blanket implementation of additive semigroup laws for additive
/// semigroup implementations.
///
impl<S: AddSemigroup> AddSemigroupLaws for S {}


///
/// Numeric laws of additive semigroups.
///
pub trait NumAddSemigroupLaws: NumEq + AddSemigroup {

  /// The numeric additive _associativity_ axiom.
  fn num_associativity(&self, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
      self.add(&x.add(y)).num_eq(&self.add(x).add(y), eps)
  }
}


///
/// Blanket implementation of numeric additive semigroup laws for
/// additive semigroup implementations.
///
impl<S: NumEq + AddSemigroup> NumAddSemigroupLaws for S {}


///
/// Define `AddSemigroup` implementations for numeric types.
///
macro_rules! numeric_add_semigroup {
  ($type:ty) => {
    impl AddSemigroup for $type {}
  };

  ($type:ty, $($others:ty),+) => {
    numeric_add_semigroup! {$type}
    numeric_add_semigroup! {$($others),+}
  };
}


// Numeric additive semigroups.
numeric_add_semigroup! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64
}


///
/// 0-tuples form an additive semigroup.
///
impl AddSemigroup for () {}


///
/// 1-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup> AddSemigroup for (A,) {}


///
/// 2-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup, B: AddSemigroup> AddSemigroup for (A, B) {}


///
/// 3-tuples form an additive semigroup when their items do.
///
impl<A: AddSemigroup, B: AddSemigroup, C: AddSemigroup> AddSemigroup for (A, B, C) {}


///
/// Define `AddSemigroup` implementations for arrays. Maybe not needed
/// if Rust had _const_ _generics_.
///
macro_rules! array_add_semigroup {
  ($size:expr) => {
    impl<T: Copy + AddSemigroup> AddSemigroup for [T; $size] {}
  };

  ($size:expr, $($others:expr),+) => {
    array_add_semigroup! {$size}
    array_add_semigroup! {$($others),+}
  };
}


// Array additive semigroup types.
array_add_semigroup! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;



