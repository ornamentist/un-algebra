//!
//! (Non-strict) partial order relations.
//!
//! A (non-strict) _partial_ _order_ on a set `S` is a _binary_
//! _relation_ `R` on `S` (written `xRx` for ∀x ∈ `S`), with
//! _anti-symmetric_, _reflexive_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```text
//! ∀x, y, z ∈ S
//!
//! Reflexive: xRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! Anti-symmetric: xRy Λ yRx ⇒ x = y.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a (non-strict) partial
//! order.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic (non-strict) _partial_ _order_ _relation_.
///
pub trait PartialOrder: PartialEq {}


///
/// Laws of partial orders.
///
pub trait PartialOrderLaws: PartialOrder {

  /// The property of partial order _antisymmetry_.
  fn antisymmetry(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    imply(f(x, y) && f(y, x), x == y)
  }


  /// The property of partial order _reflexivity_.
  fn reflexivity(f: &Rel<Self>, x: &Self) -> bool {
    f(x, x)
  }


  /// The property of partial order _transitivity_.
  fn transitivity(f: &Rel<Self>, x: &Self, y: &Self, z: &Self) -> bool {
    imply(f(x, y) && f(y, z), f(x, z))
  }
}


///
/// Blanket implementation of partial order laws for partial order
/// relation implementations.
///
impl<P: PartialOrder> PartialOrderLaws for P {}


///
/// Numeric laws of partial orders.
///
pub trait NumPartialOrderLaws: NumEq + PartialOrder {

  /// The numeric property of partial order _antisymmetry_.
  fn num_antisymmetry(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, x, eps), x.num_eq(y, eps))
  }


  /// The numeric property of partial order _reflexivity_.
  fn num_reflexivity(f: &NumRel<Self>, x: &Self, eps: &Self::Eps) -> bool {
    f(x, x, eps)
  }


  /// The numeric property of partial order _transitivity_.
  fn num_transitivity(f: &NumRel<Self>, x: &Self, y: &Self, z: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
  }
}


///
/// Blanket implementation of numeric partial order laws for partial
/// order implementations.
///
impl<P: NumEq + PartialOrder> NumPartialOrderLaws for P {}


///
/// IEEE floating point types implement partial order relations.
///
impl PartialOrder for f32 {}
impl PartialOrder for f64 {}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
