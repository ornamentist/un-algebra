//!
//! Generative tests for _inequality_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  fn ne_symmetry_f32((x, y) in (F32, F32)) {
    prop_assert!(NumInequalityLaws::num_symmetry(&f32::num_ne, &x, &y, &F32_EPS))
  }


  #[test]
  fn ne_symmetry_f64((x, y) in (F64, F64)) {
    prop_assert!(NumInequalityLaws::num_symmetry(&f64::num_ne, &x, &y, &F64_EPS))
  }


  #[test]
  fn irreflexivity_f32(x in F32) {
    prop_assert!(NumInequalityLaws::num_irreflexivity(&f32::num_ne, &x, &F32_EPS))
  }


  #[test]
  fn irreflexivity_f64(x in F64) {
    prop_assert!(NumInequalityLaws::num_irreflexivity(&f64::num_ne, &x, &F64_EPS))
  }
}
