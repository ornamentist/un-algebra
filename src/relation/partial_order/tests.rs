//!
//! Generative tests for _partial_ _order_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn antisymmetry_le_f32((x, y) in (F32, F32)) {
    prop_assert!(NumPartialOrderLaws::num_antisymmetry(&f32::num_le, &x, &y, &F32_EPS))
  }


  #[test]
  fn antisymmetry_ge_f32((x, y) in (F32, F32)) {
    prop_assert!(NumPartialOrderLaws::num_antisymmetry(&f32::num_ge, &x, &y, &F32_EPS))
  }


  #[test]
  fn reflexivity_le_f32(x in F32) {
    prop_assert!(NumPartialOrderLaws::num_reflexivity(&f32::num_le, &x, &F32_EPS))
  }


  #[test]
  fn reflexivity_ge_f32(x in F32) {
    prop_assert!(NumPartialOrderLaws::num_reflexivity(&f32::num_ge, &x, &F32_EPS))
  }


  #[test]
  fn transitivity_le_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(NumPartialOrderLaws::num_transitivity(&f64::num_le, &x, &y, &z, &F64_EPS))
  }


  #[test]
  fn transitivity_ge_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(NumPartialOrderLaws::num_transitivity(&f64::num_ge, &x, &y, &z, &F64_EPS))
  }
}
