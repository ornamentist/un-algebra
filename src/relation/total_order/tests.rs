//!
//! Generative tests for _total_ _order_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn connexity_le_f64((x, y) in (F64, F64)) {
    prop_assert!(NumTotalOrderLaws::num_connexity(&f64::num_le, &x, &y, &F64_EPS))
  }


  #[test]
  fn connexity_ge_f64((x, y) in (F64, F64)) {
    prop_assert!(NumTotalOrderLaws::num_connexity(&f64::num_ge, &x, &y, &F64_EPS))
  }
}
