//!
//! Binary relation types.
//!
//! Type aliases for working with algebraic and numeric _binary_
//! _relations_.
//!
//! # References
//!
//! See [references] for a formal definition of a binary relation.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::numeric::*;


///
/// An algebraic binary relation on a set of `T` items.
///
pub type Rel<T> = dyn Fn(&T, &T) -> bool;


///
/// A numeric binary relation on a set of `T` items. The generic type
/// parameter should be `<T: NumEq>`, but generic bounds in this
/// position are unsupported in Rust.
///
pub type NumRel<T> = dyn Fn(&T, &T, &<T as NumEq>::Eps) -> bool;




