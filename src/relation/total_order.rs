//!
//! Total order relations.
//!
//! A _total_ _order_ on a set `S` is a _binary_ _relation_ `R` on `S`
//! (written `xRx` for ∀x ∈ `S`), with _anti-symmetric_, _connex_ and
//! _transitive_ properties.
//!
//! # Properties
//!
//! ```text
//! ∀x, y, z ∈ S
//!
//! Connex: xRy ∨ yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! Anti-symmetric: xRy Λ yRx ⇒ x = y.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a total order.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _total_ _order_ _relation_.
///
pub trait TotalOrder: PartialEq {}


///
/// Laws of total orders.
///
pub trait TotalOrderLaws: TotalOrder {

  /// The property of total order _connexity_.
  fn connexity(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    f(x, y) || f(y, x)
  }


  /// The property of (total order) _anti_ _symmetry_.
  fn antisymmetry(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    imply(f(x, y) && f(y, x), x == y)
  }


  /// The property of total order _transitivity_.
  fn transitivity(f: &Rel<Self>, x: &Self, y: &Self, z: &Self) -> bool {
    imply(f(x, y) && f(y, z), f(x, z))
  }
}


///
/// Blanket implementation of total order laws for total order relation
/// implementations.
///
impl<S: TotalOrder> TotalOrderLaws for S {}


///
/// Numeric laws of total orders.
///
pub trait NumTotalOrderLaws: NumEq + TotalOrder {

  /// The numeric property of total order _connexity_.
  fn num_connexity(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    f(x, y, eps) || f(y, x, eps)
  }


  /// The  numeric property of total order _anti_ _symmetry_.
  fn antisymmetry(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, x, eps), x.num_eq(y, eps))
  }


  /// The numeric property of total order _transitivity_.
  fn num_transitivity(f: &NumRel<Self>, x: &Self, y: &Self, z: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
  }
}


///
/// Blanket implementation of numeric total order laws for total order
/// implementations.
///
impl<S: NumEq + TotalOrder> NumTotalOrderLaws for S {}


///
/// IEEE floating point types implement total order relations.
///
impl TotalOrder for f32 {}
impl TotalOrder for f64 {}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
