//!
//! Generative tests for _strict_ _partial_ _order_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn irreflexivity_le_f64(x in F64) {
    prop_assert!(NumStrictOrderLaws::num_irreflexivity(&f64::num_lt, &x, &F64_EPS))
  }


  #[test]
  fn irreflexivity_ge_f64(x in F64) {
    prop_assert!(NumStrictOrderLaws::num_irreflexivity(&f64::num_gt, &x, &F64_EPS))
  }


  #[test]
  fn transitivity_lt_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(NumStrictOrderLaws::num_transitivity(&f64::num_lt, &x, &y, &z, &F64_EPS))
  }


  #[test]
  fn transitivity_gt_f64((x, y, z) in (F64, F64, F64)) {
    prop_assert!(NumStrictOrderLaws::num_transitivity(&f64::num_gt, &x, &y, &z, &F64_EPS))
  }
}
