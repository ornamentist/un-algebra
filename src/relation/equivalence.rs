//!
//! Equivalence relations.
//!
//! An _equivalence_ _relation_ on a set `S` is a _binary_ _relation_
//! `R` on `S` (written `xRx` for ∀x ∈ `S`), with _reflexive_,
//! _symmetric_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```text
//! ∀x, y, z ∈ S
//!
//! Reflexive: xRx.
//! Symmetric: xRy ⇒ yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an equivalence relation.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _equivalence_ _relation_.
///
pub trait Equivalence: Sized {}


///
/// Laws of equivalence relations.
///
pub trait EquivalenceLaws: Equivalence {

  /// The property of equivalence _symmetry_.
  fn symmetry(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    imply(f(x, y), f(y, x))
  }


  /// The property of equivalence _reflexivity_.
  fn reflexivity(f: &Rel<Self>, x: &Self) -> bool {
    f(x, x)
  }


  /// The property of equivalence _transitivity_.
  fn transitivity(f: &Rel<Self>, x: &Self, y: &Self, z: &Self) -> bool {
    imply(f(x, y) && f(y, z), f(x, z))
  }
}


///
/// Blanket implementation of equivalence relation laws for equivalence
/// relation implementations.
///
impl<E: Equivalence> EquivalenceLaws for E {}


///
/// Numeric laws of equivalence relations.
///
pub trait NumEquivalenceLaws: NumEq + Equivalence {

  /// The numeric property of equivalence _symmetry_.
  fn num_symmetry(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps), f(y, x, eps))
  }


  /// The numeric property of equivalence _reflexivity_.
  fn num_reflexivity(f: &NumRel<Self>, x: &Self, eps: &Self::Eps) -> bool {
    f(x, x, eps)
  }


  /// The numeric property of equivalence _transitivity_.
  fn num_transitivity(f: &NumRel<Self>, x: &Self, y: &Self, z: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
  }
}


///
/// Blanket implementation of numeric equivalence relation laws for
/// equivalence relation implementations.
///
impl<E: NumEq + Equivalence> NumEquivalenceLaws for E {}


///
/// IEEE floating point types implement equivalence relations.
///
impl Equivalence for f32 {}
impl Equivalence for f64 {}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
