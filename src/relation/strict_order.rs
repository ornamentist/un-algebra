//!
//! Strict partial order relations.
//!
//! A _strict_ _partial_ _order_ (or _strict_ _order_) on a set `S` is a
//! _binary_ _relation_ `R` on `S` (written `xRx` for ∀x ∈ `S`), with
//! _asymmetric_, _irreflexive_ and _transitive_ properties.
//!
//! # Properties
//!
//! ```text
//! ∀x, y, z ∈ S
//!
//! Irreflexive: ¬xRx.
//! Asymmetric: xRy ⇒ ¬yRx.
//! Transitive: xRy Λ yRz ⇒ xRz.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of a strict partial
//! order.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _strict_ _partial_ _order_ _relation_.
///
pub trait StrictOrder: Sized {}


///
/// Laws of strict orders.
///
pub trait StrictOrderLaws: StrictOrder {

  /// The property of strict order _asymmetry_.
  fn asymmetry(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    imply(f(x, y), !f(y, x))
  }


  /// The property of strict order _irreflexivity_.
  fn irreflexivity(f: &Rel<Self>, x: &Self) -> bool {
    !f(x, x)
  }


  /// The property of strict order _transitivity_.
  fn transitivity(f: &Rel<Self>, x: &Self, y: &Self, z: &Self) -> bool {
    imply(f(x, y) && f(y, z), f(x, z))
  }
}


///
/// Blanket implementation of strict order laws for strict order
/// relation implementations.
///
impl<S: StrictOrder> StrictOrderLaws for S {}


///
/// Numeric laws of strict orders.
///
pub trait NumStrictOrderLaws: NumEq + StrictOrder {

  /// The numeric property of strict order _asymmetry_.
  fn num_asymmetry(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps), !f(y, x, eps))
  }


  /// The numeric property of strict order _irreflexivity_.
  fn num_irreflexivity(f: &NumRel<Self>, x: &Self, eps: &Self::Eps) -> bool {
    !f(x, x, eps)
  }


  /// The numeric property of strict order _transitivity_.
  fn num_transitivity(f: &NumRel<Self>, x: &Self, y: &Self, z: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps) && f(y, z, eps), f(x, z, eps))
  }
}


///
/// Blanket implementation of numeric strict order laws for numeric
/// strict order implementations.
///
impl<S: NumEq + StrictOrder> NumStrictOrderLaws for S {}


///
/// IEEE floating point types implement strict partial order relations.
///
impl StrictOrder for f32 {}
impl StrictOrder for f64 {}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
