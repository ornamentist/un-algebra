//!
//! Inequality relations.
//!
//! An _inequality_ _relation_ on a set `S` is a _binary_ _relation_ `R`
//! on `S` (written `xRx` for ∀x ∈ `S`), with _irreflexive_ and
//! _symmetric_ properties.
//!
//! # Properties
//!
//! ```text
//! ∀x, y ∈ S
//!
//! Irreflexive: not xRx.
//! Symmetric: xRy ⇒ yRx.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an inequality relation.
//!
#![doc(include = "../../doc/references.md")]

pub use crate::helpers::*;
pub use crate::numeric::*;
pub use super::relation::*;


///
/// An algebraic _inequality_ _relation_.
///
pub trait Inequality: Sized {}


///
/// Laws of inequality relations.
///
pub trait InequalityLaws: Inequality {

  /// The property of inequality _symmetry_.
  fn symmetry(f: &Rel<Self>, x: &Self, y: &Self) -> bool {
    imply(f(x, y), f(y, x))
  }


  /// The property of inequality _irreflexivity_.
  fn irreflexivity(f: &Rel<Self>, x: &Self) -> bool {
    !f(x, x)
  }
}


///
/// Blanket implementation of inequality relation laws for inequality
/// relation implementations.
///
impl<I: Inequality> InequalityLaws for I {}


///
/// Numeric laws of inequality relations.
///
pub trait NumInequalityLaws: NumEq + Inequality {

  /// The numeric property of inequality _symmetry_.
  fn num_symmetry(f: &NumRel<Self>, x: &Self, y: &Self, eps: &Self::Eps) -> bool {
    imply(f(x, y, eps), f(y, x, eps))
  }


  /// The numeric property of inequality _irreflexivity_.
  fn num_irreflexivity(f: &NumRel<Self>, x: &Self, eps: &Self::Eps) -> bool {
    !f(x, x, eps)
  }
}


///
/// Blanket implementation of numeric inequality relation laws for
/// inequality relation implementations.
///
impl<I: NumEq + Inequality> NumInequalityLaws for I {}


///
/// IEEE floating point types implement inequality relations.
///
impl Inequality for f32 {}
impl Inequality for f64 {}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;
