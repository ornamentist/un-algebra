//!
//! Generative tests for _field_ trait implementers.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {

  // Allow more value rejects to find invertible tuples.
  #![proptest_config(config_with(300, 4000))]


  #[test]
  fn left_inverse_f32(x in TF32) {
    prop_assume!(x.is_invertible());

    prop_assert!(x.num_left_inverse(&F32_EPS))
  }


  #[test]
  fn left_inverse_f64_t1(x in TF64) {
    prop_assume!((x,).is_invertible());

    prop_assert!((x,).num_left_inverse(&F64_EPS))
  }


  #[test]
  fn left_inverse_f64_a1(x in TF64) {
    prop_assume!([x].is_invertible());

    prop_assert!([x].num_left_inverse(&F64_EPS))
  }


  #[test]
  fn right_inverse_f64(x in TF64) {
    prop_assume!(x.is_invertible());

    prop_assert!(x.num_right_inverse(&F64_EPS))
  }


  #[test]
  fn left_inverse_f32_t2(xs in [TF32, TF32]) {
    prop_assume!(xs.is_invertible());

    prop_assert!(xs.num_left_inverse(&F32_EPS))
  }


  #[test]
  fn left_inverse_f32_a2(xs in [TF32, TF32]) {
    prop_assume!(xs.is_invertible());

    prop_assert!(xs.num_left_inverse(&F32_EPS))
  }


  #[test]
  fn add_cancellation_f64([x, y, z] in [TF64, TF64, TF64]) {
    prop_assert!(x.num_add_cancellation(&y, &z, &F64_EPS))
  }


  #[test]
  fn mul_cancellation_f32([x, y, z] in [TF32, TF32, TF32]) {
    prop_assert!(x.num_mul_cancellation(&y, &z, &F32_EPS))
  }


  #[test]
  fn mul_cancellation_f64_t1([x, y, z] in [TF32, TF32, TF32]) {
    prop_assert!((x,).num_mul_cancellation(&(y,), &(z,), &F32_EPS))
  }


  #[test]
  fn mul_cancellation_f32_a1([x, y, z] in [TF32, TF32, TF32]) {
    prop_assert!([x].num_mul_cancellation(&[y], &[z], &F32_EPS))
  }


  #[test]
  fn zero_cancellation_f32([x, y] in [TF32, TF32]) {
    prop_assert!(x.num_zero_cancellation(&y, &F32_EPS))
  }


  #[test]
  fn zero_cancellation_f64_t1([x, y] in [TF32, TF32]) {
    prop_assert!((x,).num_zero_cancellation(&(y,), &F32_EPS))
  }


  #[test]
  fn zero_cancellation_f32_a1([x, y] in [TF32, TF32]) {
    prop_assert!([x].num_zero_cancellation(&[y], &F32_EPS))
  }
}
