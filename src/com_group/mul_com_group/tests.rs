//!
//! Generative tests for _multiplicative_ _commutative_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn commutivity_f32([x, y] in [TF32, TF32]) {
    prop_assert!(x.num_commutivity(&y, &F32_EPS))
  }


  #[test]
  fn commutivity_f64([x, y] in [TF64, TF64]) {
    prop_assert!(x.num_commutivity(&y, &F64_EPS))
  }
}
