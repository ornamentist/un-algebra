//!
//! Commutative group modules.
//!
//! The `com_group` module provides sub-modules for abstract algebraic
//! _commutative_ _groups_. It also provides sub-modules for _additive_
//! or _multiplicative_ _commutative_ _groups_.
//!
pub mod com_group;
pub mod add_com_group;
pub mod mul_com_group;


// Make sub-modules visible in this module.
pub use self::com_group::*;
pub use self::add_com_group::*;
pub use self::mul_com_group::*;

