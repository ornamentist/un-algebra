//!
//! Generative tests for _additive_ _commutative_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn commutivity_i32((x, y) in any::<(i32, i32)>()) {
    prop_assert!(x.commutivity(&y))
  }


  #[test]
  fn commutivity_i16_t2([xs, ys] in any::<[(i16, i16); 2]>()) {
    prop_assert!(xs.commutivity(&ys))
  }


  #[test]
  fn commutivity_i16_a2([xs, ys] in any::<[[i16; 2]; 2]>()) {
    prop_assert!(xs.commutivity(&ys))
  }


  #[test]
  fn commutivity_f32([x, y] in [TF32, TF32]) {
    prop_assert!(x.num_commutivity(&y, &F32_EPS))
  }


  #[test]
  fn commutivity_f64([x, y] in [TF64, TF64]) {
    prop_assert!(x.num_commutivity(&y, &F64_EPS))
  }
}
