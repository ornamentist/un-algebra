//!
//! Re-exports of `un-algebra` public modules.
//!
//! This module can be wildcard-imported, i.e., `use`
//! `un_algebra::prelude::*;`.
//!

// Helper modules.
pub use crate::helpers::*;
pub use crate::numeric::*;


// Algebraic structures.
pub use crate::relation::*;
pub use crate::magma::*;
pub use crate::quasigroup::*;
pub use crate::semigroup::*;
pub use crate::monoid::*;
pub use crate::group::*;
pub use crate::com_group::*;
pub use crate::ring::*;
pub use crate::com_ring::*;
pub use crate::field::*;

