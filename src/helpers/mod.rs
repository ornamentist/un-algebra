//!
//! Helper methods for `un_algebra` modules.
//!
//! The `helpers` module provides unified "helper" traits and methods to
//! other `un_algebra` modules. Currently, these provide functionality
//! missing in Rust (e.g. mapping over homogeneous structures like
//! arrays and tuples).
//!
pub mod logic;
pub mod sequence;


// Make sub-modules visible in this module.
pub use self::logic::*;
pub use self::sequence::*;




