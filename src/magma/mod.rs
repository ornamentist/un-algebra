//!
//! Magma modules.
//!
//! The `magma` module provides sub-modules for abstract algebraic
//! _magmas_. It also provides sub-modules for _additive_ or
//! _multiplicative magmas_.
//!
pub mod magma;
pub mod add_magma;
pub mod mul_magma;


// Make sub-modules visible in this module.
pub use self::magma::*;
pub use self::add_magma::*;
pub use self::mul_magma::*;
