//!
//! Generative tests for _additive_ _magmas_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn closure_u16((x, y) in any::<(u16, u16)>()) {
    prop_assert!(x.closure(&y))
  }


  #[test]
  fn closure_u64_t1((x, y) in any::<(u64, u64)>()) {
    prop_assert!((x,).closure(&(y,)))
  }


  #[test]
  fn closure_u32_a1((x, y) in any::<(u64, u64)>()) {
    prop_assert!([x].closure(&[y]))
  }


  #[test]
  fn closure_u8_t2([xs, ys] in any::<[(u8, u8); 2]>()) {
    prop_assert!(xs.closure(&ys))
  }


  #[test]
  fn closure_i8_a2([xs, ys] in any::<[[u8; 2]; 2]>()) {
    prop_assert!(xs.closure(&ys))
  }


  #[test]
  fn closure_i64((x, y) in any::<(i64, i64)>()) {
    prop_assert!(x.closure(&y))
  }


  #[test]
  fn closure_isize_t3([xs, ys] in any::<[(isize, isize, isize); 2]>()) {
    prop_assert!(xs.closure(&ys))
  }


  #[test]
  fn closure_isize_a3([xs, ys] in any::<[[i32; 3]; 2]>()) {
    prop_assert!(xs.closure(&ys))
  }


  #[test]
  fn closure_f32([x, y] in [TF32, TF32]) {
    prop_assert!(x.num_closure(&y, &F32_EPS))
  }


  #[test]
  fn closure_f64([x, y] in [TF64, TF64]) {
    prop_assert!(x.num_closure(&y, &F64_EPS))
  }
}
