//!
//! Additive magmas.
//!
//! An algebraic _additive_ _magma_ is a set `S`, equipped with a
//! _binary_ _operation_ `+`, called _addition_. `S` is _closed_ under
//! addition.
//!
//! # Axioms
//!
//! ```text
//! Closure: ∀x, y ∈ S, x + y ∈ S.
//! ```
//!
//! # References
//!
//! See [references] for a formal definition of an additive magma.
//!
#![doc(include = "../../doc/references.md")]

use crate::numeric::*;
use crate::helpers::*;


///
/// An algebraic _additive magma_.
///
pub trait AddMagma: Sized + PartialEq {

  /// Binary _addition_ operation.
  fn add(&self, other: &Self) -> Self;
}


///
/// Laws of additive magmas. The _closure_ axiom defined here is
/// guaranteed by Rust's type system and is implemented only for
/// completeness.
///
pub trait AddMagmaLaws: AddMagma {

  /// The _closure_ axiom.
  fn closure(&self, _x: &Self) -> bool {
    true
  }
}


///
/// Blanket implementation of additive magma laws for additive magma
/// implementations.
///
impl<M: AddMagma> AddMagmaLaws for M {}


///
/// Numeric laws of additive magmas. The _closure_ axiom defined here is
/// guaranteed by Rust's type system and is implemented only for
/// completeness.
///
pub trait NumAddMagmaLaws: NumEq + AddMagma {

  /// The numeric _closure_ axiom.
  fn num_closure(&self, _x: &Self, _eps: &Self::Eps) -> bool {
    true
  }
}


///
/// Blanket implementation of numeric additive magma laws for additive magma
/// implementations.
///
impl<M: NumEq + AddMagma> NumAddMagmaLaws for M {}


///
/// Define `AddMagma` implementations for integer types. Probably not
/// needed if Rust had an `Integer` super-trait.
///
macro_rules! integer_add_magma {
  ($type:ty) => {

    impl AddMagma for $type {

      /// We use "wrapping" add to avoid overflow and guarantee the
      /// closure axiom.
      fn add(&self, other: &Self) -> Self {
        self.wrapping_add(*other)
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    integer_add_magma! {$type}
    integer_add_magma! {$($others),+}
  };
}


// AddMagma implementation for integer types.
integer_add_magma! {
  u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize
}


///
/// Define `AddMagma` implementations for floating point types. Probably
/// not needed if Rust had a `Float` super-trait.
///
macro_rules! float_add_magma {
  ($type:ty) => {

    impl AddMagma for $type {

      /// magma addition is just floating point addition.
      fn add(&self, other: &Self) -> Self {
        *self + *other
      }
    }
  };

  ($type:ty, $($others:ty),+) => {
    float_add_magma! {$type}
    float_add_magma! {$($others),+}
  };
}


// Floating point additive magma types.
float_add_magma! {
  f32, f64
}


///
/// 0-tuples form an additive magma.
///
impl AddMagma for () {

  /// Addition can only yield a `()`.
  fn add(&self, _: &Self) -> Self {}
}


///
/// 1-tuples form an additive magma when their items do.
///
impl<A: AddMagma> AddMagma for (A,) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), )
  }
}


///
/// 2-tuples form an additive magma when their items do.
///
impl<A: AddMagma, B: AddMagma> AddMagma for (A, B) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), self.1.add(&other.1))
  }
}


///
/// 3-tuples form an additive magma when their items do.
///
impl<A: AddMagma, B: AddMagma, C: AddMagma> AddMagma for (A, B, C) {

  /// Addition is by matching element.
  fn add(&self, other: &Self) -> Self {
    (self.0.add(&other.0), self.1.add(&other.1), self.2.add(&other.2))
  }
}


///
/// Define `AddMagma` implementations for arrays. Maybe not needed if
/// Rust had _const_ _generics_.
///
macro_rules! array_add_magma {
  ($size:expr) => {
    impl<T: Copy + AddMagma> AddMagma for [T; $size] {

      // Delegate to `Sequence` trait map_with function.
      fn add(&self, other: &Self) -> Self {
        self.map_with(other, &|x, y| x.add(y))
      }
    }
  };

  ($size:expr, $($others:expr),+) => {
    array_add_magma! {$size}
    array_add_magma! {$($others),+}
  };
}


// Array additive magma types.
array_add_magma! {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
}


// Module unit tests are in a sub-module.
#[cfg(test)]
mod tests;

