//!
//! Algebraic ring modules.
//!
//! The `ring` module provides sub-modules for abstract algebraic
//! _rings_.
//!
pub mod ring;


// Make sub-modules visible in this module.
pub use self::ring::*;

