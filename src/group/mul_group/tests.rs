//!
//! Generative tests for _multiplicative_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn left_inverse_f32(x in TF32) {
    prop_assume!(x.is_invertible());

    prop_assert!(x.num_left_inverse(&F32_EPS))
  }


  #[test]
  fn left_inverse_f64_t1(x in TF64) {
    prop_assume!((x,).is_invertible());

    prop_assert!((x,).num_left_inverse(&F64_EPS))
  }


  #[test]
  fn left_inverse_f32_a1(x in TF32) {
    prop_assume!([x].is_invertible());

    prop_assert!([x].num_left_inverse(&F32_EPS))
  }


  #[test]
  fn right_inverse_f64_t1(x in TF64) {
    prop_assume!((x,).is_invertible());

    prop_assert!((x,).num_right_inverse(&F64_EPS))
  }
}
