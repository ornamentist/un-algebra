//!
//! Generative tests for _additive_ _groups_.
//!
use super::*;
use crate::tests::*;


#[cfg(test)]
proptest! {
  #![proptest_config(standard())]


  #[test]
  fn left_inverse_i32(x in any::<i32>()) {
    prop_assert!(x.left_inverse())
  }


  #[test]
  fn left_inverse_i16_t1(x in any::<i16>()) {
    prop_assert!((x,).left_inverse())
  }


  #[test]
  fn left_inverse_i8_a1(x in any::<i8>()) {
    prop_assert!([x].left_inverse())
  }


  #[test]
  fn right_inverse_i16(x in any::<i16>()) {
    prop_assert!(x.right_inverse())
  }


  #[test]
  fn right_inverse_isize_t2(xs in any::<(isize, isize)>()) {
    prop_assert!(xs.right_inverse())
  }


  #[test]
  fn right_inverse_i64_a2(xs in any::<[i64; 2]>()) {
    prop_assert!(xs.right_inverse())
  }


  #[test]
  fn left_inverse_f64(x in TF64) {
    prop_assert!(x.num_left_inverse(&F64_EPS))
  }


  #[test]
  fn right_inverse_f32(x in TF32) {
    prop_assert!(x.num_right_inverse(&F32_EPS))
  }
}
