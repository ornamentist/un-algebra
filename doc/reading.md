# Abstract Algebra References

I found the following references useful in creating the `un_algebra`
crate. This is far from an exhaustive list, I'm sure there are many
other fine references available.


## Wikipedia

An overview of commonly used abstract algebraic structures, their
relationships and axioms, and further references:

https://en.wikipedia.org/w/index.php?title=Magma_(algebra)


A higher-level overview of abstract algebraic structures, grouped by
structure family and number and kinds of operations:

https://en.wikipedia.org/wiki/Algebraic_structure


Another higher-level overview of abstract algebraic structures, with
some overlap with the other Wikipedia references above:

https://en.wikipedia.org/wiki/Outline_of_algebraic_structures


A useful introduction to relations on sets:

https://en.wikipedia.org/wiki/Binary_relation



## Textbooks

I found this abstract algebra textbook to be clear and approachable for
a non-mathematician, although there are almost certainly other textbooks that fit that description too:

    @Book{GAL-2017,
      author    = {Gallian, Joseph},
      title     = {Contemporary abstract algebra},
      publisher = {Cengage Learning},
      year      = {2017},
      address   = {Boston, MA},
      isbn      = {978-1305657960}
    }


## Rust crates

The `alga` Rust crate implements most commonly used algebraic structures and is much better suited to a production environment than `un_algebra`:

https://crates.io/crates/alga


## Haskell libraries

Here's an alternate Haskell _prelude_ with in-depth support for
algebraic and mathematical structures.

https://hackage.haskell.org/package/numeric-prelude


## Purescript libraries

The Purescript prelude has comprehensive support for algebraic
structures in its numeric hierarchy. This in-depth documentation
explains the rationale behind it:

https://a-guide-to-the-purescript-numeric-hierarchy.readthedocs.io


## Video lessons

I found Bill Shillito's "Introduction to Higher Mathematics" video
series very approachable, in fact, some of the best video mathematical
exposition I've seen:

https://www.youtube.com/watch?v=CMWFmjlB8v0&list=PLZzHxk_TPOStgPtqRZ6KzmkUQBQ8TSWVX


## Other sites

A concise summary of mathematical structures "equations" (called
_axioms_ and _properties_ in `un_algebra`). Each equation is given a
recognised name:

http://math.chapman.edu/~jipsen/structures/doku.php/equations


A reference site for a large number of abstract algebraic structures,
with a brief summary of each structure's definition, examples and
notable properties:

http://math.chapman.edu/~jipsen/structures/doku.php/
