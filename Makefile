# Perform Rust (i.e. Cargo) commands at the command line (Gnu Make).


# Assume cargo is installed in our home directory.
CARGO :=~/.cargo/bin/cargo


# Cargo parallel jobs limit.
JOBS := 32


# Cargo test threads limit.
THREADS := 32


# Print make targets and exit.
.PHONY: help
help:
	$(info )
	$(info Make Targets:)
	$(info   check            -- Check source syntax.)
	$(info   build            -- Build sources and examples.)
	$(info   docs             -- Build and open documentation.)
	$(info   tests            -- Build and run tests and examples.)
	$(info   tests-release    -- Build and run release tests and examples.)
	$(info   examples         -- Build and run examples.)
	$(info   examples-release -- Build and run release examples.)
	$(info   lint             -- Clippy lint sources.)
	$(info   lint-summary     -- Clippy lint summary.)
	@exit


# Use cargo check to check crate syntax.
.PHONY: check
check:
	${CARGO} check --jobs ${JOBS} --lib --examples --tests


# Use cargo build to build crate source and examples.
.PHONY: build
build:
	${CARGO} build --jobs ${JOBS} --examples


# Use cargo doc to create and view crate documentation.
.PHONY: docs
docs:
	${CARGO} clean --doc
	${CARGO} doc --no-deps --open


# Use cargo test to run crate and example tests.
.PHONY: tests
tests:
	${CARGO} test --jobs ${JOBS} --tests -- --test-threads=${THREADS}


# Use cargo test to run (release) crate and example tests.
.PHONY: tests-release
tests-release:
	${CARGO} test --jobs ${JOBS} --tests --release -- --test-threads=${THREADS}


# Use cargo test to run crate example tests only.
.PHONY: examples
examples:
	${CARGO} test --jobs ${JOBS} --examples -- --test-threads=16


# Use cargo test to run (release) crate example tests only.
.PHONY: examples-release
examples-release:
	${CARGO} test --jobs ${JOBS} --examples --release -- --test-threads=${THREADS}


# Use clippy to do extra-pedantic linting of crate source.
.PHONY: lint
lint:
	${CARGO} clean -p un_algebra
	${CARGO} clippy --all-targets -- \
	  -W clippy::pedantic \
	  -W clippy::nursery \
	  -W clippy::style \
	  -W clippy::cargo \
	  -W clippy::correctness \
	  -A clippy::module_inception \
    -A clippy::module_name_repetitions


# Summarize clippy lints by kind to the command line.
.PHONY: lint-summary
lint-summary:
	make lint 2>&1 | grep '^warning' | sort | uniq

